import React from "react";
import LoginPage from "../components/LoginPage";

describe("login page", () => {
  it("renders div", () => {
    const wrapper = shallow(<LoginPage />);
    expect(wrapper.find("div")).toHaveLength(1);
  });
});
