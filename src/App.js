import "./App.css";
import LoginPage from "./components/LoginPage";
import { Provider } from "react-redux";
import { persistor, store } from "./store";
import { PersistGate } from "redux-persist/integration/react";
import ChatPage from "./components/ChatPage";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import ModalComponent from "./components/ModalComponent";
import { ToastContainer } from "react-toastify";

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <Switch>
            <Route path="/auth">
              <LoginPage form="login" />
            </Route>
            <Route path="/registration">
              <LoginPage form="registration" />
            </Route>
            <Route path="/forgot_pass">
              <LoginPage form="forgot_pass" />
            </Route>
            <Route path="/chat">
              <ModalComponent />
              <ChatPage />
            </Route>
            <Route path="/">
              <Redirect to={"/auth"} />
            </Route>
          </Switch>
        </BrowserRouter>
      </PersistGate>
      <ToastContainer
        position="top-right"
        autoClose={5000}
        hideProgressBar
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss={false}
        draggable
        pauseOnHover
      />
    </Provider>
  );
}

export default App;
