import * as types from "../actions/types";

const defaultState = {
  loading: true,
  avatar:
    "https://cdn.gamesubject.com/img/avatars/1f0e3dad9990/e45ee7ce7e88/751d31dd6b56/860d5c834d67894e2cab.jpg",
  name: "",
  phrases: [],
  google: false,
};

export const userDataReducer = (state = defaultState, action) => {
  switch (action.type) {
    case types.FETCH_USER_SUCCESS:
      return {
        ...state,
        ...action.data.users[0],
      };

    case types.FETCH_USER_ERROR:
      return {
        ...state,
        ...action.data,
      };
    case types.SIGN_IN_VIA_GOOGLE:
      return {
        ...state,
        google: true,
        loading: false,
        ...action.data,
      };
    case types.USER_LOGOUT:
      return defaultState;

    case types.LOGIN_USER:
      return {
        ...state,
        error: null,
      };

    case types.FETCH_USER_INFO_SUCCESS:
      return {
        ...state,
        ...action.data,
        loading: false,
      };

    case types.UPDATE_PHRASES_SETTINGS:
      return {
        ...state,
        greeting: action.greeting,
        phrases: action.phrases,
      };
    case types.UPDATE_PROFILE_SETTINGS:
      return {
        ...state,
        name: action.name,
        avatar: action.avatar,
      };

    default:
      return state;
  }
};
