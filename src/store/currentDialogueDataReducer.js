import * as types from "../actions/types";

const defaultstate = {};

export const currentDialogueDataReducer = (state = defaultstate, action) => {
  switch (action.type) {
    case types.SET_ACTIVE_DIALOGUE:
      return {
        data: action.data,
        activeTab: action.activeTab,
        id: action.id,
      };
    case types.CLOSE_MODAL:
      return {
        ...state,
        activeTab: action.tab,
      };
    case types.UPDATE_MESSAGES_SUCCESS:
      return {
        ...state,
        data: action.data,
      };
    case types.FINISH_FOCUSED_DIALOGUE:
      return defaultstate;
    default:
      return state;
  }
};
