import { applyMiddleware, combineReducers, createStore } from "redux";
import createSagaMiddleware from "redux-saga";
import rootSaga from "../sagas/index";
import { loginReducer } from "./loginReducer";
import { composeWithDevTools } from "redux-devtools-extension";
import { reducer as formReducer } from "redux-form";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { userDataReducer } from "./userDataReducer";
import { menuReducer } from "./menuReducer";
import { dataReducer } from "./dataReducer";
import { currentDialogueDataReducer } from "./currentDialogueDataReducer";
import { modalReducer } from "./modalReducer";

const persistConfig = {
  key: "auth",
  storage: storage,
  whitelist: "auth",
};
const sagaMiddleware = createSagaMiddleware();
const rootReducer = combineReducers({
  auth: loginReducer,
  form: formReducer,
  user: userDataReducer,
  menu: menuReducer,
  data: dataReducer,
  currData: currentDialogueDataReducer,
  modal: modalReducer,
});
const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(
  persistedReducer,
  composeWithDevTools(applyMiddleware(sagaMiddleware))
);
export const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);
