// @flow
import * as types from "../actions/types";

type State = {
  typedEmail: string,
  token?: ?string,

}

const defaultState = {
  typedEmail: "",
};

export const loginReducer = (state:State = defaultState, action: Object) : State => {
  switch (action.type) {
    case types.LOGIN_USER_SUCCESS:
      return {
        ...state,
        token: action.data.idToken,
      };
    case types.SIGN_IN_VIA_GOOGLE:
      return {
        ...state,
        token: action.data.token,
      };

    case types.LOGIN_USER_ERROR:
      return {
        ...state,
        token: null,
      };
    case types.LOGIN_USER:
      return {
        ...state,
        typedEmail: action.user.email,
      };
    case types.REGISTRATION_OR_RESTORATION_ATTEMPT:
      return {
        ...state,
        typedEmail: action.mail,
      };

    case types.USER_LOGOUT:
      return {
        ...state,
        token: null,
      };

    default:
      return state;
  }
};
