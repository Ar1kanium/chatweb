import * as types from "../actions/types";

const defaultState = {
  status: "closed",
  type: null,
};

export const modalReducer = (state = defaultState, action) => {
  switch (action.type) {
    case types.OPEN_SETTINGS:
      return {
        ...state,
        status: "opened",
        type: "settings",
      };
    case types.DIALOGUE_ACTION:
      return {
        ...state,
        status: "loading",
        type: "dialogueAction",
      };
    case types.DIALOGUE_ACTION_SUCCESS:
      return {
        ...state,
        status: "success",
      };
    case types.DIALOGUE_ACTION_ERROR:
      return {
        ...state,
        status: "error",
      };
    case types.OPEN_IMAGE_MODAL:
      return {
        ...state,
        status: "opened",
        type: "sendImage",
      };
    case types.OPEN_LANDSCAPE_MODAL:
      return {
        ...state,
        status: "opened",
        type: "landscapeOrientation",
      };
    case types.CLOSE_MODAL:
      return {
        ...state,
        status: "closed",
        type: null,
      };
    default:
      return state;
  }
};
