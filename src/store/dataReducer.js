import * as types from "../actions/types";


const defaultState = {};

export const dataReducer = (state = defaultState, action) => {
  const response = action.data ? action.data : null;
  switch (action.type) {
    case types.FETCH_DATA_SUCCESS:
      return {
        response,
      };

    case types.FETCH_DATA_ERROR:
      return {
        response,
      };

    case types.TRANSFER_FINISHED_DIALOGUES:
      const newState = {};
      Object.keys(state.response)
        .filter((elem) => {
          return !action.finishedDialogues.hasOwnProperty(elem);
        })
        .forEach((el) => (newState[el] = state.response[el]));
      return {
        response: newState,
      };

    default:
      return state;
  }
};
