// @flow
import * as types from "../actions/types";

type State = {
  activeTab: ?string,
  loading: boolean,
  error: boolean,
  search: boolean,
  view: string,
  id?: ?string,
}

const defaultState : State = {
  activeTab: null,
  loading: false,
  error: false,
  search: false,
  view: "default",
};

export const menuReducer = (state: State = defaultState, action : Object) : State => {
  switch (action.type) {
    case types.SET_ACTIVE_TAB:
      return {
        ...state,
        loading: true,
        error: false,
        activeTab: action.activeTab,
        search: false,
      };
    case types.SEARCH_DATA:
      return {
        ...state,
        loading: true,
        error: false,
        activeTab: action.activeTab,
        search: true,
      };
    case types.FETCH_DATA_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
      };
    case types.FETCH_DATA_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
      };
    case types.SET_ACTIVE_DIALOGUE:
      return {
        ...state,
        id: action.id,
      };
    case types.DEFAULT_VIEW_MODE:
      return {
        ...state,
        view: "default",
      };
    case types.MOBILE_VIEW_MODE:
      return {
        ...state,
        view: "mobile",
      };
    case types.FINISH_FOCUSED_DIALOGUE:
      return {
        ...state,
        id: null,
      };
    default:
      return state;
  }
};
