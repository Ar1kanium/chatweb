import { DATABASE_URL } from "../consts/consts";
import firebase from "firebase/app";
import "firebase/database";
import { toast } from "react-toastify";

export async function fetchDialoguesData(payload) {
  const PATH =
    payload.activeTab !== "pending"
      ? `${payload.activeTab}/${payload.id}.json`
      : `${payload.activeTab}.json`;
  const AUTH = `?auth=${payload.token}`;
  let DATA_API_ENDPOINT = `${DATABASE_URL}/${PATH}/${AUTH}`;
  if (payload.page) {
    const QUERY = `?orderBy="timestamp"&limitToLast=${
      payload.page * 15
    }&print=pretty`;
    DATA_API_ENDPOINT = `${DATABASE_URL}/${PATH}/${QUERY}${AUTH}`;
  }
  const response = await fetch(DATA_API_ENDPOINT);
  const data = await response.json();
  if (payload.activeTab === "pending") {
    Object.keys(data).forEach((key) => {
      if (data[key]["operator"]) delete data[key];
    });
  }
  if (payload.search) {
    delete data["fake"];
    Object.keys(data).forEach((key) => {
      const clName = data[key].clientname.toLowerCase();
      const topicName = data[key].topic.toLowerCase();
      const searchStr = payload.search.toLowerCase();
      if (
        clName.indexOf(searchStr) === -1 &&
        topicName.indexOf(searchStr) === -1 &&
        Object.values(data[key].messages).reduce((acc, curr) => {
          if (
            curr.message.toLowerCase().indexOf(payload.search.toLowerCase()) ===
            -1
          )
            return acc;
          return 0;
        }, -1) === -1
      )
        delete data[key];
    });
  }
  Object.keys(data).forEach((key) => {
    if (data[key]["messages"] && key !== "fake") {
      let tempMesArr = [];
      let lastMessage = "";
      Object.keys(data[key]["messages"]).forEach((_key) => {
        tempMesArr.push(+data[key]["messages"][_key]["timestamp"]);
        if (
          +data[key]["messages"][_key]["timestamp"] === Math.max(...tempMesArr)
        )
          lastMessage = data[key]["messages"][_key]["message"];
      });
      data[key]["lastMessage"] = lastMessage;
    }
  });

  return data;
}

export async function actionsWithDialogues(payload) {
  const AUTH = `?auth=${payload.token}`;
  if (payload.activeTab === "finished") {
    const PATH = `saved/${payload.localId}/${payload.id}.json`;
    const DATA_API_ENDPOINT = `${DATABASE_URL}/${PATH}/${AUTH}`;
    const response = await fetch(DATA_API_ENDPOINT, {
      method: "PUT",
      body: JSON.stringify(payload.data[payload.id]),
    });
    const data = await response.json();
    return data;
  }
  if (payload.activeTab === "saved") {
    const PATH = `saved/${payload.localId}/${payload.id}.json`;
    const DATA_API_ENDPOINT = `${DATABASE_URL}/${PATH}/${AUTH}`;
    const response = await fetch(DATA_API_ENDPOINT, {
      method: "DELETE",
    });
    const data = await response.json();
    return data;
  }
  if (payload.activeTab === "pending") {
    if (!payload.data["messages"]["fOpMes"]["message"])
      payload.data["messages"]["fOpMes"]["message"] = "Диалог взят оператором";
    const PATH = `pending/${payload.id}.json`;
    const DATA_API_ENDPOINT = `${DATABASE_URL}/${PATH}/${AUTH}`;
    const response = await fetch(DATA_API_ENDPOINT, {
      headers: {
        "X-Firebase-ETag": true,
      },
    });
    const ETAG = response.headers.get("Etag");
    const dialData = response.json();
    if (dialData["operator"])
      throw new Error("Диалог уже взят другим оператором");
    await fetch(DATA_API_ENDPOINT, {
      headers: {
        "if-match": ETAG,
      },
      method: "PUT",
      body: JSON.stringify({ operator: payload.localId }),
    });
    await fetch(
      `${DATABASE_URL}/active/${payload.localId}/${payload.id}.json/${AUTH}`,
      {
        method: "PUT",
        body: JSON.stringify(payload.data),
      }
    );
  }
}

export async function updateDialoguesMessages(payload) {
  const AUTH = `?auth=${payload.token}`;
  const PATH = `active/${payload.localId}/${payload.id}.json`;
  const DATA_API_ENDPOINT = `${DATABASE_URL}/${PATH}/${AUTH}`;
  const resp = await fetch(DATA_API_ENDPOINT);
  const data = await resp.json();
  return { [payload.id]: data };
}

export async function updatePhrases(payload) {
  const AUTH = `?auth=${payload.token}`;
  const PATH = `users/${payload.localId}.json`;
  const DATA_API_ENDPOINT = `${DATABASE_URL}/${PATH}/${AUTH}`;
  const resp = await fetch(DATA_API_ENDPOINT, {
    method: "PATCH",
    body: JSON.stringify({
      greeting: payload.greeting,
      phrases: payload.phrases,
    }),
  });
  const data = await resp.json();
  return data;
}

export async function updateProfile(avatar, name, token, localId) {
  const AUTH = `?auth=${token}`;
  const PATH = `users/${localId}.json`;
  const DATA_API_ENDPOINT = `${DATABASE_URL}/${PATH}/${AUTH}`;
  const resp = await fetch(DATA_API_ENDPOINT, {
    method: "PATCH",
    body: JSON.stringify({ avatar: avatar, name: name }),
  });
  const data = await resp.json();
  return data;
}

export async function sendMessage(id, token, localId, data) {
  const PATH = `active/${localId}/${id}/messages.json`;
  const AUTH = `?auth=${token}`;
  const DATA_API_ENDPOINT = `${DATABASE_URL}/${PATH}/${AUTH}`;
  const response = await fetch(DATA_API_ENDPOINT, {
    method: "POST",
    body: JSON.stringify(data),
  });
  await response.json();
  const PATH_UPDATE_TIME = `active/${localId}/${id}.json`;
  const DATA_API_ENDPOINT_UPDATE_TIME = `${DATABASE_URL}/${PATH_UPDATE_TIME}/${AUTH}`;
  const updTime = await fetch(DATA_API_ENDPOINT_UPDATE_TIME, {
    method: "PATCH",
    body: JSON.stringify({ timestamp: { ".sv": "timestamp" } }),
  });
  const resp = await updTime.json();
  return resp;
}

export async function sendImage(id, token, localId, image) {
  const PATH = `active/${localId}/${id}/messages.json`;
  const AUTH = `?auth=${token}`;
  const DATA_API_ENDPOINT = `${DATABASE_URL}/${PATH}/${AUTH}`;
  const response = await fetch(DATA_API_ENDPOINT, {
    method: "POST",
    body: JSON.stringify({
      author: "operator",
      image: "image",
      message: image,
      timestamp: { ".sv": "timestamp" },
    }),
  });
  await response.json();
  const PATH_UPDATE_TIME = `active/${localId}/${id}.json`;
  const DATA_API_ENDPOINT_UPDATE_TIME = `${DATABASE_URL}/${PATH_UPDATE_TIME}/${AUTH}`;
  const updTime = await fetch(DATA_API_ENDPOINT_UPDATE_TIME, {
    method: "PATCH",
    body: JSON.stringify({ timestamp: { ".sv": "timestamp" } }),
  });
  const resp = await updTime.json();
  return resp;
}

export async function transferMessages(payload) {
  let finishedMessagesQuantity = Object.keys(payload.finishedDialogues).length;
  let counter = 0;
  let clientString = "";
  for (const [key, value] of Object.entries(payload.finishedDialogues)) {
    await firebase
      .database()
      .ref(`finished/${payload.localId}/${key}`)
      .set(value);
    await firebase.database().ref(`active/${payload.localId}/${key}`).remove();
    switch (finishedMessagesQuantity - counter) {
      case 1:
        clientString += value["clientname"];
        break;
      case 2:
        clientString += `${value["clientname"]} и `;
        break;
      default:
        clientString += `${value["clientname"]}, `;
    }
    counter++;
  }
  await toast.info(
    finishedMessagesQuantity - 1
      ? `${clientString} завершили диалоги`
      : `${clientString} завершил(-a) диалог`
  );
}
