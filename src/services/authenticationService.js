import { API_KEY, DATABASE_URL } from "../consts/consts";
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/database";
import { toast } from "react-toastify";

export const loginUserService = (request) => {
  const LOGIN_API_ENDPOINT = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${API_KEY}`;

  const parameters = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      returnSecureToken: true,
      email: request.user.email,
      password: request.user.password,
    }),
  };

  return fetch(LOGIN_API_ENDPOINT, parameters).then((response) =>
    response.json()
  );
};

export const fetchUserData = (request) => {
  const LOGIN_API_ENDPOINT = `https://identitytoolkit.googleapis.com/v1/accounts:lookup?key=${API_KEY}`;
  const parameters = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ idToken: request.token }),
  };

  return fetch(LOGIN_API_ENDPOINT, parameters).then((response) =>
    response.json()
  );
};

export async function fetchUserDataInfo(payload) {
  const PATH = `users/${payload.localId}.json`;
  const AUTH = `?auth=${payload.token}`;
  const DATA_API_ENDPOINT = `${DATABASE_URL}/${PATH}/${AUTH}`;
  const response = await fetch(DATA_API_ENDPOINT);
  const data = await response.json();
  return data;
}

export async function registerUserEmail({ firstName, email, password }) {
  try {
    const userCredential = await firebase
      .auth()
      .createUserWithEmailAndPassword(email, password);
    const uid = userCredential.user.uid;
    await Promise.all([
      firebase.database().ref(`users/${uid}`).set({ name: firstName }),
      firebase
        .database()
        .ref(`active/${uid}`)
        .set({ fake: { fake: "fake" } }),
      firebase
        .database()
        .ref(`saved/${uid}`)
        .set({ fake: { fake: "fake" } }),
      firebase
        .database()
        .ref(`finished/${uid}`)
        .set({ fake: { fake: "fake" } }),
    ]);
    toast.success("Вы успешно зарегистрировались!");
  } catch (error) {
    toast.error(error.message);
  }
}

export async function signInWithGoogle() {
  try {
    const provider = new firebase.auth.GoogleAuthProvider();
    const result = await firebase.auth().signInWithPopup(provider);
    const displayName = result.user.displayName;
    const uid = result.user.uid;
    const usersSnap = await firebase.database().ref(`/users`).get();
    const users = await usersSnap.val();
    if (Object.keys(users).indexOf(uid) === -1) {
      await Promise.all([
        firebase.database().ref(`users/${uid}`).set({ name: displayName }),
        firebase
          .database()
          .ref(`active/${uid}`)
          .set({ fake: { fake: "fake" } }),
        firebase
          .database()
          .ref(`saved/${uid}`)
          .set({ fake: { fake: "fake" } }),
        firebase
          .database()
          .ref(`finished/${uid}`)
          .set({ fake: { fake: "fake" } }),
      ]);
    }
    const userInfoSnap = await firebase.database().ref(`users/${uid}`).get();
    const userInfo = await userInfoSnap.val();
    userInfo["localId"] = uid;
    userInfo["token"] = await firebase.auth().currentUser.getIdToken(true);
    return userInfo;
  } catch (e) {
    toast.error(e.message);
  }
}

export async function forgotPassword(email) {
  try {
    await firebase.auth().sendPasswordResetEmail(email);
    toast.success(`Отправлено письмо на адрес ${email}`);
  } catch (e) {
    toast.error(e.message);
  }
}
