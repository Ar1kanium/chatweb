import { all } from "redux-saga/effects";
import { watchAuthUser } from "./loginSaga";
import { watchUserData } from "./userDataSaga";
import { watchFetchingData } from "./fetchingDataSaga";
import { watchDialoguesActions } from "./dialoguesActionsSaga";
import {
  watchUserDataInfo,
  watchUserPhrasesUpdateSage,
} from "./userDataInfoSaga";
import { watchUpdatingMessages } from "./updatingMessagesSaga";
import { watchFinishedDialogues } from "./finishedDialoguesSaga";

export default function* rootSaga() {
  yield all([
    watchAuthUser(),
    watchUserData(),
    watchFetchingData(),
    watchDialoguesActions(),
    watchUserDataInfo(),
    watchUpdatingMessages(),
    watchUserPhrasesUpdateSage(),
    watchFinishedDialogues(),
  ]);
}
