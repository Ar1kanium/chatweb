import { call, put, takeLatest } from "redux-saga/effects";
import { fetchUserData } from "../services/authenticationService";
import * as types from "../actions/types";
import { fetchUserSuccess, userLogout } from "../actions/actionCreators";

function* userDataSaga(payload) {
  try {
    const response = yield call(fetchUserData, payload);
    if (response.error) {
      yield put(userLogout());
    } else yield put(fetchUserSuccess(response));
  } catch (error) {
    yield put(userLogout());
  }
}

export function* watchUserData() {
  yield takeLatest(types.FETCH_USER, userDataSaga);
}
