import * as types from "../actions/types";
import { call, takeLeading } from "redux-saga/effects";
import { transferMessages } from "../services/databaseService";

function* finishedDialoguesSaga(payload) {
  yield call(transferMessages, payload);
}

export function* watchFinishedDialogues() {
  yield takeLeading(types.TRANSFER_FINISHED_DIALOGUES, finishedDialoguesSaga);
}
