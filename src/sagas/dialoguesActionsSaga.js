import * as types from "../actions/types";
import { call, put, takeLatest } from "redux-saga/effects";
import { actionsWithDialogues } from "../services/databaseService";
import {
  dialogueActionSuccess,
  dialogueActionError,
} from "../actions/actionCreators";

function* dialoguesActionsSaga(payload) {
  try {
    yield call(actionsWithDialogues, payload);
    yield put(dialogueActionSuccess());
  } catch {
    yield put(dialogueActionError());
  }
}

export function* watchDialoguesActions() {
  yield takeLatest(types.DIALOGUE_ACTION, dialoguesActionsSaga);
}
