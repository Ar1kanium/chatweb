import * as types from "../actions/types";
import { call, put, takeLatest } from "redux-saga/effects";
import { updateDialoguesMessages } from "../services/databaseService";
import { updateMessagesSuccess } from "../actions/actionCreators";

function* updatingMessagesSaga(payload) {
  try {
    const response = yield call(updateDialoguesMessages, payload);
    yield put(updateMessagesSuccess(response));
  } catch {}
}

export function* watchUpdatingMessages() {
  yield takeLatest(types.UPDATE_MESSAGES, updatingMessagesSaga);
}
