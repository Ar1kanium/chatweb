import * as types from "../actions/types";
import { call, put, takeLatest } from "redux-saga/effects";
import { fetchDialoguesData } from "../services/databaseService";
import { fetchDataError, fetchDataSuccess } from "../actions/actionCreators";

function* fetchingDataSaga(payload) {
  try {
    const response = yield call(fetchDialoguesData, payload);
    if (response && !response.error) {
      yield put(fetchDataSuccess(response));
    } else {
      yield put(fetchDataError(response));
    }
  } catch (error) {
    yield put(fetchDataError(error));
  }
}

export function* watchFetchingData() {
  yield takeLatest(
    [types.SEARCH_DATA, types.FETCH_MORE_DIALOGUES],
    fetchingDataSaga
  );
}
