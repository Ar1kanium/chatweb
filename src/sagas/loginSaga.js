import { call, put, takeLatest } from "redux-saga/effects";
import * as types from "../actions/types";
import { loginUserService } from "../services/authenticationService";
import { loginUserError, loginUserSuccess } from "../actions/actionCreators";
import { startAsyncValidation, stopAsyncValidation } from "redux-form";

function* loginSaga(payload) {
  try {
    const response = yield call(loginUserService, payload);
    yield put(startAsyncValidation("login"));
    if (response.error) {
      yield put(loginUserError(response));
      yield put(
        stopAsyncValidation("login", {
          _error: "Неправильно введен логин или пароль",
        })
      );
    } else {
      yield put(loginUserSuccess(response));
      yield put(stopAsyncValidation("login"));
    }
  } catch (error) {
    yield put(loginUserError(error));
  }
}

export function* watchAuthUser() {
  yield takeLatest(types.LOGIN_USER, loginSaga);
}
