import * as types from "../actions/types";
import { call, put, takeLatest } from "redux-saga/effects";
import { closeModal, fetchUserInfoSuccess } from "../actions/actionCreators";
import { fetchUserDataInfo } from "../services/authenticationService";
import { updatePhrases } from "../services/databaseService";

function* userDataInfoSaga(payload) {
  const response = yield call(fetchUserDataInfo, payload);
  yield put(fetchUserInfoSuccess(response));
}

function* userPhrasesUpdateSage(payload) {
  yield call(updatePhrases, payload);
  yield put(closeModal());
}

export function* watchUserDataInfo() {
  yield takeLatest(types.FETCH_USER_INFO, userDataInfoSaga);
}

export function* watchUserPhrasesUpdateSage() {
  yield takeLatest(types.UPDATE_PHRASES_SETTINGS, userPhrasesUpdateSage);
}
