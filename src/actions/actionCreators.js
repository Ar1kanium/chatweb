import * as types from "./types";

export const loginUserAction = (user) => {
  return {
    type: types.LOGIN_USER,
    user,
  };
};

export const loginUserSuccess = (data) => {
  return {
    type: types.LOGIN_USER_SUCCESS,
    data,
  };
};

export const loginUserError = (data) => {
  return {
    type: types.LOGIN_USER_ERROR,
    data,
  };
};

export const fetchUserAction = (token) => {
  return {
    type: types.FETCH_USER,
    token: token,
  };
};

export const fetchUserSuccess = (data) => {
  return {
    type: types.FETCH_USER_SUCCESS,
    data,
  };
};

export const fetchUserError = (data) => {
  return {
    type: types.FETCH_USER_ERROR,
    data,
  };
};

export const userLogout = () => {
  return {
    type: types.USER_LOGOUT,
  };
};

export const setActiveTab = (tab) => {
  return {
    type: types.SET_ACTIVE_TAB,
    activeTab: tab,
  };
};

export const fetchMoreDialogues = (tab, token, id, page) => {
  return {
    type: types.FETCH_MORE_DIALOGUES,
    activeTab: tab,
    token: token,
    id: id,
    page: page,
  };
};

export const searchData = (str, token, id, tab) => {
  return {
    type: types.SEARCH_DATA,
    token: token,
    id: id,
    search: str,
    activeTab: tab,
  };
};

export const fetchDataSuccess = (data) => {
  return {
    type: types.FETCH_DATA_SUCCESS,
    data,
  };
};

export const fetchDataError = () => {
  return {
    type: types.FETCH_DATA_ERROR,
  };
};

export const setActiveDialogue = (id, data, activeTab) => {
  return {
    type: types.SET_ACTIVE_DIALOGUE,
    id: id,
    data: data,
    activeTab: activeTab,
  };
};

export const dialogueAction = (activeTab, id, token, localId, data) => {
  return {
    type: types.DIALOGUE_ACTION,
    activeTab: activeTab,
    id: id,
    token: token,
    localId: localId,
    data: data,
  };
};

export const dialogueActionSuccess = () => {
  return {
    type: types.DIALOGUE_ACTION_SUCCESS,
  };
};

export const dialogueActionError = () => {
  return {
    type: types.DIALOGUE_ACTION_ERROR,
  };
};

export const closeModal = (tab) => {
  return {
    type: types.CLOSE_MODAL,
    tab: tab,
  };
};

export const fetchUserInfo = (localId, token) => {
  return {
    type: types.FETCH_USER_INFO,
    localId: localId,
    token: token,
  };
};

export const fetchUserInfoSuccess = (data) => {
  return {
    type: types.FETCH_USER_INFO_SUCCESS,
    data,
  };
};

export const updateMessages = (localId, token, id) => {
  return {
    type: types.UPDATE_MESSAGES,
    localId: localId,
    token: token,
    id: id,
  };
};

export const updateMessagesSuccess = (data) => {
  return {
    type: types.UPDATE_MESSAGES_SUCCESS,
    data,
  };
};

export const openSettings = () => {
  return {
    type: types.OPEN_SETTINGS,
  };
};

export const updateProfileSettings = (avatar, name, token, localId) => {
  return {
    type: types.UPDATE_PROFILE_SETTINGS,
    avatar,
    name,
    token,
    localId,
  };
};

export const updatePhrasesSettings = (greeting, phrases, token, localId) => {
  return {
    type: types.UPDATE_PHRASES_SETTINGS,
    greeting,
    phrases,
    token,
    localId,
  };
};

export const openImageModal = () => {
  return {
    type: types.OPEN_IMAGE_MODAL,
  };
};

export const openLandscapeModal = () => {
  return {
    type: types.OPEN_LANDSCAPE_MODAL,
  };
};

export const defaultViewMode = () => {
  return {
    type: types.DEFAULT_VIEW_MODE,
  };
};

export const mobileViewMode = () => {
  return {
    type: types.MOBILE_VIEW_MODE,
  };
};

export const transferFinishedDialogues = (finishedDialogues, localId) => {
  return {
    type: types.TRANSFER_FINISHED_DIALOGUES,
    finishedDialogues: finishedDialogues,
    localId,
  };
};

export const finishFocusedDialogue = () => {
  return {
    type: types.FINISH_FOCUSED_DIALOGUE,
  };
};

export const registrationOrRestorationAttempt = (mail) => {
  return {
    type: types.REGISTRATION_OR_RESTORATION_ATTEMPT,
    mail,
  };
};

export const signInViaGoogle = (data) => {
  return {
    type: types.SIGN_IN_VIA_GOOGLE,
    data,
  };
};
