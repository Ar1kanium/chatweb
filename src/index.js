import React from "react";
import ReactDOM from "react-dom";
import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";
import 'bootstrap/dist/css/bootstrap.min.css';
import "react-toastify/dist/ReactToastify.css";
import App from "./App";
import firebase from "firebase/app";

firebase.initializeApp({ apiKey: process.env.REACT_APP_API_KEY,
                                authDomain: "ar1chat.firebaseapp.com",
                                databaseURL: "https://ar1chat-default-rtdb.europe-west1.firebasedatabase.app",
                                projectId: "ar1chat",
                                storageBucket: "ar1chat.appspot.com",
                                messagingSenderId: "808866375623",
                                appId: "1:808866375623:web:5e4e70e196097daf101be2",})


Sentry.init({
  dsn: process.env.REACT_APP_SENTRY_DSN,
  integrations: [new Integrations.BrowserTracing()],
  tracesSampleRate: 1.0,
});

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);
