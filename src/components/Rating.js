import React from "react";
import { useSelector } from "react-redux";
import { faStar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Rating = ({ dialogueId }) => {
  const starCount = useSelector(
    (state) => state.data.response[dialogueId]["rating"]
  );
  if (!starCount) return <></>;
  const ratingArr = [];
  for (let _ = 0; _ < starCount; _++) {
    ratingArr.push(
      <FontAwesomeIcon
        key={_}
        icon={faStar}
        className="left-menu__rating_star"
      />
    );
  }

  return <div className="left-menu__rating">{ratingArr}</div>;
};

export default Rating;
