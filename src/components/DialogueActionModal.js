import React from "react";
import { useSelector } from "react-redux";

const DialogueActionModal = () => {
  const currData = useSelector((state) => state.currData);
  const { status } = useSelector((state) => state.modal);
  const text = {
    loading: {
      finished: "Добавляем диалог в сохранённые...",
      saved: "Удаляем диалог из сохранённых...",
      pending: "Входим в диалог с клиентом...",
    },
    success: {
      finished: "Диалог успешно сохранён",
      saved: "Диалог успешно удалён",
      pending: "Готово!",
    },
    failure: {
      finished: "Что-то пошло не так 😥",
      saved: "Что-то пошло не так 😥",
      pending: "Диалог уже взят другим оператором",
    },
  };
  const [response, setResponse] = React.useState(
    text[status][currData.activeTab]
  );

  // eslint-disable-next-line
  React.useEffect(() => {
    setResponse(text[status][currData.activeTab]);
  });

  return (
    <div>
      {response}
      <div className="background__lcircle"></div>
      <div className="background__scircle"></div>
      <div className="background__mcircle"></div>
    </div>
  );
};

export default DialogueActionModal;
