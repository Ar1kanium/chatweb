import React from "react";
import { Form, Field } from "react-final-form";
import { Link, Redirect } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { registrationOrRestorationAttempt } from "../actions/actionCreators";
import { forgotPassword } from "../services/authenticationService";

const ForgotPassForm = () => {
  const [redirect, setRedirect] = React.useState(false);
  const dispatch = useDispatch();
  const initMail = useSelector((state) => state.auth.typedEmail);
  const onSubmit = (values) => {
    dispatch(registrationOrRestorationAttempt(values["email"]));
    forgotPassword(values["email"]).then(() => setRedirect(true));
  };
  const required = (value) => (value ? undefined : "Обязательное поле");

  if (redirect) return <Redirect to="/auth" />;

  return (
    <Form
      onSubmit={onSubmit}
      initialValues={{ email: initMail ? initMail : "" }}
      render={({ handleSubmit, form, values }) => (
        <form className="auth-form" onSubmit={handleSubmit}>
          <span className="auth-form__title">Восстановить пароль</span>
          <Field name="email" validate={required}>
            {({ input, meta }) => (
              <>
                <label className="auth-form__label" htmlFor="email">
                  Электронная почта
                </label>
                <input
                  {...input}
                  className="auth-form__input"
                  id="email"
                  name="email"
                  type="email"
                />
                {meta.error && meta.touched && (
                  <span className="auth-form__error">{meta.error}</span>
                )}
              </>
            )}
          </Field>
          <button className="auth-form__button" type="submit">
            Отправить письмо
          </button>
          <div className="auth-form__link_container">
            <Link className="auth-form__link" to="/auth">
              Авторизация
            </Link>
            <Link className="auth-form__link" to="/registration">
              Регистрация
            </Link>
          </div>
        </form>
      )}
    />
  );
};

export default ForgotPassForm;
