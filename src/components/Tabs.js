import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchMoreDialogues, setActiveTab } from "../actions/actionCreators";
import {
  faCheckCircle,
  faCloudDownloadAlt,
  faComments,
  faMehRollingEyes,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const Tabs = () => {
  const dispatch = useDispatch();
  const activeTab = useSelector((state) => state.menu.activeTab);
  const token = useSelector((state) => state.auth.token);
  const localId = useSelector((state) => state.user.localId);
  const handleClick = (tab) => {
    dispatch(setActiveTab(tab));
    dispatch(fetchMoreDialogues(tab, token, localId, 1));
  };

  return (
    <div className="left-menu__tab_container">
      <FontAwesomeIcon
        icon={faComments}
        className={
          activeTab === "active"
            ? "left-menu__tab left-menu__tab_active"
            : "left-menu__tab"
        }
        onClick={() => handleClick("active")}
      />
      <FontAwesomeIcon
        icon={faMehRollingEyes}
        className={
          activeTab === "pending"
            ? "left-menu__tab left-menu__tab_active"
            : "left-menu__tab"
        }
        onClick={() => handleClick("pending")}
      />
      <FontAwesomeIcon
        icon={faCloudDownloadAlt}
        className={
          activeTab === "saved"
            ? "left-menu__tab left-menu__tab_active"
            : "left-menu__tab"
        }
        onClick={() => handleClick("saved")}
      />
      <FontAwesomeIcon
        icon={faCheckCircle}
        className={
          activeTab === "finished"
            ? "left-menu__tab left-menu__tab_active"
            : "left-menu__tab"
        }
        onClick={() => handleClick("finished")}
      />
    </div>
  );
};

export default Tabs;
