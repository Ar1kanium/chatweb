import React from "react";
import {
  faPaperPlane,
  faPaperclip,
  faGrinBeam,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useDispatch, useSelector } from "react-redux";
import { useCombobox } from "downshift";
import EmojiPicker from "./EmojiPicker";
import { usePubNub } from "pubnub-react";
import { usePrev } from "../hooks/usePrev";
import {
  dialogueAction,
  openImageModal,
  updateMessages,
} from "../actions/actionCreators";
import { sendMessage } from "../services/databaseService";

const TextInput = () => {
  const dispatch = useDispatch();
  const phrases = useSelector((state) => state.user.phrases);
  const greeting = useSelector((state) => state.user.greeting);
  const currDial = useSelector((state) => state.currData.id);
  const currTab = useSelector((state) => state.currData.activeTab);
  const token = useSelector((state) => state.auth.token);
  const localId = useSelector((state) => state.user.localId);
  const dialData = useSelector((state) => state.currData.data[currDial]);
  const menuId = useSelector((state) => state.menu.id);
  const [inputItems, setInputItems] = React.useState(phrases);
  const [textInput, setTextInput] = React.useState("");
  const [typingStatus, setTypingStatus] = React.useState(false);
  const pubnub = usePubNub();
  const lastDial = usePrev(currDial);
  React.useEffect(() => {
    if (greeting && currTab && currTab === "pending") setTextInput(greeting);
    else setTextInput("");
    if (lastDial) pubnub.signal({ message: "false", channel: lastDial });
    return pubnub.signal({ message: "false", channel: currDial });
  }, [currDial, pubnub, lastDial, currTab, greeting]);
  React.useEffect(() => {
    if (textInput && !typingStatus) {
      setTypingStatus(true);
      pubnub.signal({ message: "true", channel: `${currDial}operator` });
    }
    if (!textInput && typingStatus) {
      setTypingStatus(false);
      pubnub.signal({ message: "false", channel: `${currDial}operator` });
    }
  }, [textInput, pubnub, typingStatus, currDial]);

  const handleChange = (e) => {
    setTextInput(e.target.value);
  };
  const handleSend = async () => {
    if (textInput && menuId) {
      await sendMessage(currDial, token, localId, {
        author: "operator",
        message: textInput,
        timestamp: { ".sv": "timestamp" },
      });
      setTextInput("");
    }
    dispatch(updateMessages(localId, token, currDial));
  };

  const handleTake = () => {
    dialData["messages"]["fOpMes"] = {
      author: "operator",
      message: textInput,
      timestamp: { ".sv": "timestamp" },
    };
    dialData["timestamp"] = { ".sv": "timestamp" };
    delete dialData["lastMessage"];
    dispatch(dialogueAction(currTab, currDial, token, localId, dialData));
  };

  const [emojiBarVisibility, setEmojiBarVisibility] = React.useState(false);
  const toggleEmojiBarVisibility = () => {
    setEmojiBarVisibility(true);
  };

  const {
    isOpen,
    getMenuProps,
    getInputProps,
    getComboboxProps,
    highlightedIndex,
    getItemProps,
  } = useCombobox({
    items: inputItems,
    stateReducer: (state, actionAndChanges) => {
      if (
        actionAndChanges.type === useCombobox.stateChangeTypes.ItemClick ||
        actionAndChanges.type === useCombobox.stateChangeTypes.InputKeyDownEnter
      ) {
        setTextInput(actionAndChanges.changes.selectedItem);
      }
      return actionAndChanges.changes;
    },
    onInputValueChange: ({ inputValue }) => {
      setInputItems(
        phrases.filter((item) =>
          item.toLowerCase().startsWith(inputValue.toLowerCase())
        )
      );
    },
  });

  const emojiRef = React.useRef(null);
  const textAreaRef = React.useRef(null);
  const pickEmoji = (emoji) => {
    setTextInput(textInput + emoji);
    textAreaRef.current.focus();
  };
  const closeEmojiBar = () => setEmojiBarVisibility(false);
  React.useEffect(() => {
    const handleClicks = (e) => {
      if (!emojiRef.current || emojiRef.current.contains(e.target)) return;
      closeEmojiBar();
    };
    document.addEventListener("mousedown", handleClicks);
    document.addEventListener("touchstart", handleClicks);

    return () => {
      document.removeEventListener("mousedown", handleClicks);
      document.removeEventListener("touchstart", handleClicks);
    };
  }, [emojiRef]);

  return (
    <>
      <div ref={emojiRef}>
        <EmojiPicker
          isActive={emojiBarVisibility}
          textInput={textInput}
          pickEmoji={pickEmoji}
        />
      </div>
      <div className="content__autocomplete">
        <ul className="content__autocomplete_list" {...getMenuProps()}>
          {isOpen &&
            inputItems.map((item, index) => (
              <li
                style={
                  highlightedIndex === index
                    ? { backgroundColor: "#327aff", color: "white" }
                    : {}
                }
                key={`${item}${index}`}
                {...getItemProps({ item, index })}
              >
                {item}
              </li>
            ))}
        </ul>
      </div>
      <div className="content__input_container" {...getComboboxProps()}>
        <textarea
          {...getInputProps({
            ref: textAreaRef,
            value: textInput,
            onChange: (e) => handleChange(e),
          })}
          placeholder="&nbsp;&nbsp;Введите текст сообщения..."
          className="content__input"
        />
        <div className="content__icons_container">
          <FontAwesomeIcon
            onClick={toggleEmojiBarVisibility}
            className="content__icon"
            icon={faGrinBeam}
          />
          <FontAwesomeIcon
            className="content__icon"
            onClick={() => dispatch(openImageModal())}
            icon={faPaperclip}
          />
          <div
            className="content__icon_container"
            onClick={currTab === "active" ? handleSend : handleTake}
          >
            <FontAwesomeIcon
              className="content__icon_send"
              icon={faPaperPlane}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default TextInput;
