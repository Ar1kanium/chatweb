import React from "react";
import Modal from "react-modal";
import { useDispatch, useSelector } from "react-redux";
import {
  closeModal,
  fetchMoreDialogues,
  openLandscapeModal,
  setActiveTab,
} from "../actions/actionCreators";
import Settings from "./Settings";
import DialogueActionModal from "./DialogueActionModal";
import ImageSender from "./ImageSender";
import LandscapeOrientation from "./LandscapeOrientation";

Modal.setAppElement(document.getElementById("root"));

const ModalComponent = () => {
  const { type } = useSelector((state) => state.modal);
  const currData = useSelector((state) => state.currData);
  const token = useSelector((state) => state.auth.token);
  const localId = useSelector((state) => state.user.localId);
  const dispatch = useDispatch();
  const payload = {
    finished: "saved",
    saved: "saved",
    pending: "active",
  };
  const dialogueAction = () => {
    dispatch(closeModal(payload[currData["activeTab"]]));
    dispatch(setActiveTab(payload[currData["activeTab"]]));
    dispatch(
      fetchMoreDialogues(payload[currData["activeTab"]], token, localId, 1)
    );
  };
  const settings = () => {
    dispatch(closeModal(currData["activeTab"]));
  };
  const sendImage = () => {
    dispatch(closeModal(currData["activeTab"]));
  };
  const landscapeOrientation = () => {};
  const hideModal = {
    dialogueAction: dialogueAction,
    settings: settings,
    sendImage: sendImage,
    landscapeOrientation: landscapeOrientation,
  };
  const typeModal = {
    dialogueAction: <DialogueActionModal />,
    settings: <Settings />,
    sendImage: <ImageSender />,
    landscapeOrientation: <LandscapeOrientation />,
  };
  const isOpen = type !== null;

  React.useEffect(() => {
    const orientationHandler = () => {
      if (window.innerWidth < 900 && window.innerWidth > window.innerHeight)
        dispatch(openLandscapeModal());
      else if (type === "landscapeOrientation") {
        dispatch(closeModal(currData["activeTab"]));
      }
    };
    window.addEventListener("resize", orientationHandler);

    return () => window.removeEventListener("resize", orientationHandler);
  });

  return type !== null ? (
    <Modal
      isOpen={isOpen}
      onRequestClose={hideModal[type]}
      className="modal_m"
      overlayClassName="modal__overlay_m"
      shouldFocusAfterRender={false}
    >
      {typeModal[type]}
    </Modal>
  ) : (
    <></>
  );
};

export default ModalComponent;
