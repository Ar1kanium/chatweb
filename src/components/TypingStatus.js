import React from "react";
import { usePubNub } from "pubnub-react";
import { useSelector } from "react-redux";

const TypingStatus = () => {
  const [typing, setTyping] = React.useState(false);
  const dialogueId = useSelector((state) => state.currData.id);
  const activeTab = useSelector((state) => state.currData.activeTab);
  const pubnub = usePubNub();
  const handleSignal = (e) => {
    const msg = e.message;
    if (msg === "true") setTyping(true);
    if (msg === "false") setTyping(false);
  };
  React.useEffect(() => {
    pubnub.addListener({ signal: handleSignal });
    pubnub.subscribe({ channels: [`${dialogueId}client`] });
  }, [pubnub, dialogueId]);

  React.useEffect(() => {
    setTyping(false);
  }, [activeTab]);

  if (!typing || activeTab !== "active") return <></>;

  return (
    <div className="content__client_message_wrapper content__typing">
      <div className="content__typing_dot"></div>
      <div className="content__typing_dot"></div>
      <div className="content__typing_dot"></div>
    </div>
  );
};

export default TypingStatus;
