import React from 'react'
import { PopoverBody, PopoverHeader, UncontrolledPopover } from "reactstrap";
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ProjectInfo = () => {

  return(
    <>
      <FontAwesomeIcon icon={faInfoCircle} id='InfoIcon'/>
      <UncontrolledPopover trigger="legacy" placement="bottom" target="InfoIcon">
        <PopoverHeader>О проекте</PopoverHeader>
        <PopoverBody>
          Привет! Это <a href="https://gitlab.com/Ar1kanium/chatweb" rel="noreferrer" target="_blank">веб-приложение</a> техподдержки.
          Предназначено для операторов, клиентами используется <a href="https://gitlab.com/Ar1kanium/chatmobile" rel="noreferrer" target="_blank">мобильное приложение</a>.
          Разработано в целях обучения в ходе стажировки в компании Noorsoft.
          <br/><a href="mailto:ar1kanium@gmail.com">E-mail</a> для связи.
        </PopoverBody>
      </UncontrolledPopover>
    </>
  )
}

export default ProjectInfo