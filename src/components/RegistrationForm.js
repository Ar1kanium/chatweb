import React from "react";
import { Form, Field } from "react-final-form";
import { Link, Redirect } from "react-router-dom";
import { registerUserEmail } from "../services/authenticationService";
import { useDispatch, useSelector } from "react-redux";
import { registrationOrRestorationAttempt } from "../actions/actionCreators";

const RegistrationForm = () => {
  const [redirect, setRedirect] = React.useState(false);
  const dispatch = useDispatch();
  const onSubmit = (values, form) => {
    dispatch(registrationOrRestorationAttempt(values["email"]));
    registerUserEmail(values).then(() => setRedirect(true));
    form.change("password", undefined);
    form.change("passwordConf", undefined);
  };
  const required = (value) => (value ? undefined : "Обязательное поле");
  const mustHaveANumber = (value) =>
    !value.match(/\d/g) ? "Требуется хотя бы одна цифра" : undefined;
  const mustHaveCapitalLetter = (value) =>
    !value.match(/[A-Z]/g)
      ? "Требуется хотя бы одна заглавная буква"
      : undefined;
  const mustHaveLowerCaseLetter = (value) =>
    !value.match(/[a-z]/g)
      ? "Требуется хотя бы одна строчная буква"
      : undefined;
  const minLength = (value) =>
    value.length < 8 ? "Минимум 8 символов" : undefined;
  const mustBeSame = (pass) => (value) =>
    pass !== value ? "Пароли не совпадают" : undefined;
  const composeValidators =
    (...validators) =>
    (value) =>
      validators.reduce(
        (error, validator) => error || validator(value),
        undefined
      );

  const emailValidator = (value) =>
    // eslint-disable-next-line
    !value.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)
      ? "Неправильный адрес"
      : undefined;

  const initMail = useSelector((state) => state.auth.typedEmail);

  if (redirect) return <Redirect to="/auth" />;

  return (
    <Form
      onSubmit={onSubmit}
      initialValues={{ email: initMail ? initMail : "" }}
      render={({ handleSubmit, form, values }) => (
        <form className="auth-form" onSubmit={handleSubmit}>
          <span className="auth-form__title">Регистрация</span>
          <Field name="firstName" validate={required}>
            {({ input, meta }) => (
              <>
                <label className="auth-form__label" htmlFor="firstName">
                  Имя пользователя
                </label>
                <input
                  {...input}
                  className="auth-form__input"
                  id="firstName"
                  name="firstName"
                  type="firstName"
                />
                {meta.error && meta.touched && (
                  <span className="auth-form__error">{meta.error}</span>
                )}
              </>
            )}
          </Field>
          <Field
            name="email"
            validate={composeValidators(required, emailValidator)}
          >
            {({ input, meta }) => (
              <>
                <label className="auth-form__label" htmlFor="email">
                  Электронная почта
                </label>
                <input
                  {...input}
                  className="auth-form__input"
                  id="email"
                  name="email"
                  type="email"
                />
                {meta.error && meta.touched && (
                  <span className="auth-form__error">{meta.error}</span>
                )}
              </>
            )}
          </Field>
          <Field
            name="password"
            validate={composeValidators(
              required,
              mustHaveCapitalLetter,
              mustHaveANumber,
              mustHaveLowerCaseLetter,
              minLength
            )}
          >
            {({ input, meta }) => (
              <>
                <label className="auth-form__label" htmlFor="password">
                  Пароль
                </label>
                <input
                  {...input}
                  className="auth-form__input"
                  id="password"
                  name="password"
                  type="password"
                />
                {meta.error && meta.touched && (
                  <span className="auth-form__error">{meta.error}</span>
                )}
              </>
            )}
          </Field>
          <Field name="passwordConf" validate={mustBeSame(values["password"])}>
            {({ input, meta }) => (
              <>
                <label className="auth-form__label" htmlFor="password">
                  Подтверждение пароля
                </label>
                <input
                  {...input}
                  className="auth-form__input"
                  id="passwordConf"
                  name="passwordConf"
                  type="password"
                />
                {meta.error && meta.touched && (
                  <span className="auth-form__error">{meta.error}</span>
                )}
              </>
            )}
          </Field>
          <button className="auth-form__button" type="submit">
            Зарегистрироваться
          </button>
          <div className="auth-form__link_container">
            <Link className="auth-form__link" to="/auth">
              Авторизация
            </Link>
            <Link className="auth-form__link" to="/forgot_pass">
              Забыли пароль?
            </Link>
          </div>
        </form>
      )}
    />
  );
};

export default RegistrationForm;
