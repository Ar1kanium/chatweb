import React from "react";
import LoginForm from "./LoginForm";
import RegistrationForm from "./RegistrationForm";
import ForgotPassForm from "./ForgotPassForm";

const LoginPage = ({ form }) => {
  const formSelect = {
    login: <LoginForm />,
    registration: <RegistrationForm />,
    forgot_pass: <ForgotPassForm />,
  };

  return (
    <div className="wrapper__auth">
      {formSelect[form]}
      <div className="background__lcircle"></div>
      <div className="background__mcircle"></div>
      <div className="background__scircle"></div>
    </div>
  );
};

export default LoginPage;
