import React from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchMoreDialogues,
  setActiveDialogue,
  transferFinishedDialogues,
} from "../actions/actionCreators";
import Loader from "./Loader";
import InfiniteScroll from "react-infinite-scroller";
import Rating from "./Rating";

const DialoguesMenu = () => {
  const dispatch = useDispatch();
  const currDialogue = useSelector((state) => state.menu.id);
  const dialogues = useSelector((state) => state.data.response);
  const activeTab = useSelector((state) => state.menu.activeTab);
  const currActiveTab = useSelector((state) => state.currData.activeTab);
  const token = useSelector((state) => state.auth.token);
  const localId = useSelector((state) => state.user.localId);
  const loading = useSelector((state) => state.menu.loading);
  const isSearching = useSelector((state) => state.menu.search);
  React.useEffect(() => {
    const filterFinishedDialogues = () => {
      if (activeTab === "active" && !loading) {
        const finishedDialogues = {};
        Object.keys(dialogues).forEach((key) => {
          if (dialogues[key]["rating"]) finishedDialogues[key] = dialogues[key];
        });
        if (!Object.values(finishedDialogues).length) {
          return;
        }
        dispatch(transferFinishedDialogues(finishedDialogues, localId));
      }
    };
    filterFinishedDialogues();
  }, [activeTab, dialogues, loading, dispatch, localId]);
  if (!activeTab) return <></>;
  if (loading) return <Loader />;
  let keysTimestampsLM = [];
  Object.keys(dialogues).forEach((el) => {
    if (el !== "fake") keysTimestampsLM.push([el, dialogues[el].timestamp]);
  });
  keysTimestampsLM.sort((a, b) => b[1] - a[1]);
  const sortedKeys = keysTimestampsLM.map((el) => el[0]);
  const dialoguesLayout = sortedKeys.map((el) => (
    <div
      key={el}
      className={
        el === currDialogue && currActiveTab === activeTab
          ? "left-menu__dialogue_container left-menu__dialogue_container_active"
          : "left-menu__dialogue_container"
      }
      onClick={() => dispatch(setActiveDialogue(el, dialogues, activeTab))}
    >
      <div className="left-menu__dialogue_client">{`${dialogues[el].clientname}:`}</div>
      <div className="left-menu__dialogue_topic">{dialogues[el].topic}</div>
      <div className="left-menu__dialogue_last_message">
        {dialogues[el]["lastMessage"].length > 45
          ? `${dialogues[el]["lastMessage"].substr(0, 42)}...`
          : dialogues[el]["lastMessage"]}
      </div>
      <Rating dialogueId={el} />
    </div>
  ));
  if (isSearching)
    return (
      <div className="left-menu__dialogues_container">{dialoguesLayout}</div>
    );

  const handleLoadMore = (page) => {
    dispatch(fetchMoreDialogues(activeTab, token, localId, page));
  };

  return (
    <div className="left-menu__dialogues_container">
      <InfiniteScroll
        pageStart={0}
        hasMore={!dialogues.fake}
        loadMore={handleLoadMore}
        loader={<Loader key={0} />}
        useWindow={false}
      >
        {dialoguesLayout}
      </InfiniteScroll>
    </div>
  );
};

export default DialoguesMenu;
