import React from "react";
import { connect } from "react-redux";
import {
  defaultViewMode,
  fetchUserAction,
  fetchUserInfo,
  mobileViewMode,
} from "../actions/actionCreators";
import { Redirect } from "react-router-dom";
import Loader from "./Loader";
import LeftMenu from "./LeftMenu";
import Header from "./Header";
import Content from "./Content";
import ViewToggle from "./ViewToggle";

class ChatPage extends React.Component {
  state = { toggle: false };

  toggleHandler = () => {
    this.setState({ toggle: !this.state.toggle });
  };

  switchViewMode = () => {
    if (this.props.view === "default" && window.innerWidth < 900)
      this.props.mobileViewMode();
    else if (this.props.view === "mobile" && window.innerWidth >= 900)
      this.props.defaultViewMode();
  };

  componentDidMount() {
    if (!this.props.google) this.props.fetchUserAction(this.props.token);
    window.addEventListener("resize", this.switchViewMode);
    this.switchViewMode();
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (this.props.id && this.props.id !== prevProps.id) this.toggleHandler();
    if (this.props.loading && this.props.localId && !this.props.google)
      this.props.fetchUserInfo(this.props.localId, this.props.token);
    this.switchViewMode();
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.switchViewMode);
  }

  render() {
    if (!this.props.token || this.props.error) return <Redirect to={"/auth"} />;
    if (this.props.loading) {
      return (
        <div className="wrapper">
          <Loader />
        </div>
      );
    }
    return (
      <div
        className={
          this.props.view === "default"
            ? "wrapper__chat"
            : "wrapper__chat wrapper__chat_mobile"
        }
      >
        <LeftMenu menuStatus={this.state.toggle} />
        <Header />
        <Content />
        <ViewToggle
          status={this.state.toggle}
          toggleHandler={this.toggleHandler}
        />
      </div>
    );
  }
}

export default connect(
  (state) => ({
    token: state.auth.token,
    loading: state.user.loading,
    localId: state.user.localId,
    error: state.user.error,
    view: state.menu.view,
    id: state.menu.id,
    google: state.user.google,
  }),
  {
    fetchUserAction,
    fetchUserInfo,
    defaultViewMode,
    mobileViewMode,
  }
)(ChatPage);
