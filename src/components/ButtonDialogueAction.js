import React from "react";
import { faTrashAlt, faShare } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useDispatch, useSelector } from "react-redux";
import { dialogueAction } from "../actions/actionCreators";

const ButtonDialogueAction = () => {
  const dispatch = useDispatch();
  const { activeTab, id, data } = useSelector((state) => state.currData);
  const token = useSelector((state) => state.auth.token);
  const localId = useSelector((state) => state.user.localId);
  const handleClick = () => {
    dispatch(dialogueAction(activeTab, id, token, localId, data));
  };

  const switchIconName = {
    finished: [faShare, "header__icon_container"],
    saved: [faTrashAlt, "header__icon_container"],
  };

  if (!activeTab || activeTab === "active" || activeTab === "pending")
    return <></>;

  return (
    <div className={switchIconName[activeTab][1]} onClick={handleClick}>
      <FontAwesomeIcon
        className="header__icon"
        icon={switchIconName[activeTab][0]}
      />
    </div>
  );
};

export default ButtonDialogueAction;
