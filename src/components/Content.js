import React from "react";
import { useSelector } from "react-redux";
import TextInput from "./TextInput";
import Messages from "./Messages";
import Pubnub from "pubnub";
import { PubNubProvider } from "pubnub-react";

const Content = () => {
  const currDialogue = useSelector((state) => state.menu.id);
  const currActiveTab = useSelector((state) => state.currData.activeTab);
  const uid = useSelector((state) => state.user.localId);
  const pubnub = new Pubnub({
    publishKey: process.env.REACT_APP_PUBNUB_PUBLISH_KEY,
    subscribeKey: process.env.REACT_APP_PUBNUB_SUBSCRIBE_KEY,
    uuid: uid,
  });

  if (!currDialogue && currDialogue !== 0) {
    return (
      <PubNubProvider client={pubnub}>
        <div className="content">
          {currActiveTab === "active" || currActiveTab === "pending" ? (
            <TextInput />
          ) : (
            <></>
          )}
        </div>
      </PubNubProvider>
    );
  }
  return (
    <PubNubProvider client={pubnub}>
      <div className="content">
        <Messages />
        {currActiveTab === "active" || currActiveTab === "pending" ? (
          <TextInput />
        ) : (
          <></>
        )}
      </div>
    </PubNubProvider>
  );
};

export default Content;
