import React from "react";
import { Formik, Form, Field, FieldArray } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { updatePhrasesSettings } from "../actions/actionCreators";
import { faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { toast } from "react-toastify";

const DialoguesSettings = () => {
  const { localId, greeting, phrases } = useSelector((state) => state.user);
  const token = useSelector((state) => state.auth.token);
  const dispatch = useDispatch();
  const onSubmit = (values) => {
    const newPhrases = values.phrases.filter((el) => el !== "");
    const newGreeting = values.greeting;
    dispatch(updatePhrasesSettings(newGreeting, newPhrases, token, localId));
    toast.success("Заготовки ответов успешно обновлены");
  };

  return (
    <Formik initialValues={{ greeting, phrases }} onSubmit={onSubmit}>
      {({ values }) => (
        <Form className="settings__card">
          <span className="settings__title">Заготовки ответов</span>
          <label htmlFor="greeting" className="settings__label">
            Стартовая фраза
          </label>
          <Field
            type="text"
            name="greeting"
            className="settings__input"
          ></Field>
          <FieldArray
            name="phrases"
            render={(arrayHelpers) => (
              <div>
                <div>
                  <button
                    type="button"
                    className="settings__button"
                    onClick={() => arrayHelpers.push("")}
                  >
                    Добавить фразу
                  </button>
                </div>
                <div className="settings__items">
                  {values.phrases && values.phrases.length > 0 ? (
                    values.phrases.map((phrase, index) => (
                      <div key={index} className="settings__item">
                        <Field
                          className="settings__input settings__input_container"
                          name={`phrases.${index}`}
                          placeholder="Введите текст новой заготовки..."
                        />
                        <button
                          type="button"
                          className="settings__button settings__button_remove"
                          onClick={() => arrayHelpers.remove(index)}
                        >
                          <FontAwesomeIcon icon={faTrashAlt} />
                        </button>
                      </div>
                    ))
                  ) : (
                    <></>
                  )}

                  <div>
                    <button className="settings__button" type="submit">
                      {" "}
                      Обновить{" "}
                    </button>
                  </div>
                </div>
              </div>
            )}
          />
        </Form>
      )}
    </Formik>
  );
};

export default DialoguesSettings;
