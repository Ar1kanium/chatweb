import React from "react";

const LandscapeOrientation = () => {
  return (
    <div>
      Поверните устройство.
      <div className="background__lcircle"></div>
      <div className="background__scircle"></div>
      <div className="background__mcircle"></div>
    </div>
  );
};

export default LandscapeOrientation;
