import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { userLogout } from "../actions/actionCreators";
import ButtonDialogueAction from "./ButtonDialogueAction";

const Header = () => {
  const dispatch = useDispatch();
  const name = useSelector((state) => state.user.name);
  const avatar = useSelector((state) => state.user.avatar);

  return (
    <div className="header">
      <div className="header__container">
        <img src={avatar} alt="avatar" className="header__avatar" />
        <p className="header__name">Оператор {name}</p>
        <div className="header__icons_container">
          <ButtonDialogueAction />
          <div
            className="header__icon_container"
            onClick={() => dispatch(userLogout())}
          >
            <FontAwesomeIcon className="header__icon" icon={faSignOutAlt} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
