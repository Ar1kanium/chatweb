import React from "react";
import Picker from "emoji-picker-react";

const EmojiPicker = ({ pickEmoji, isActive }) => {
  const handleClick = (e, emojiObj) => {
    pickEmoji(emojiObj.emoji);
  };

  return (
    <>
      <Picker
        onEmojiClick={handleClick}
        pickerStyle={
          isActive
            ? {
                transition: "250ms",
                position: "fixed",
                right: "0px",
                bottom: "60px",
                color: "#3896ff",
              }
            : { position: "fixed", bottom: "-350px", right: "0px" }
        }
        native
      />
    </>
  );
};

export default EmojiPicker;
