import React from "react";
import Tabs from "./Tabs";
import { faEllipsisV } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import DialoguesMenu from "./DialoguesMenu";
import { useDispatch, useSelector } from "react-redux";
import debounce from "lodash.debounce";
import { openSettings, searchData } from "../actions/actionCreators";

const LeftMenu = ({ menuStatus }) => {
  const dispatch = useDispatch();
  const view = useSelector((state) => state.menu.view);
  const token = useSelector((state) => state.auth.token);
  const id = useSelector((state) => state.user.localId);
  const activeTab = useSelector((state) => state.menu.activeTab);
  const mobileViewOptions = menuStatus
    ? "left-menu left-menu_active"
    : "left-menu left-menu_inactive";
  const handleChange = debounce(async (e) => {
    if (e.target.value) {
      dispatch(searchData(e.target.value.toString(), token, id, activeTab));
    }
  }, 1000);

  return (
    <div className={view === "default" ? "left-menu" : mobileViewOptions}>
      <div className="left-menu__header">
        <span className="left-menu__title">Ar1Chat</span>
        <FontAwesomeIcon
          className="left-menu__settings"
          icon={faEllipsisV}
          onClick={() => dispatch(openSettings())}
        />
      </div>
      <input
        placeholder="Поиск..."
        type="search"
        className="left-menu__search_input"
        onChange={handleChange}
      />
      <Tabs />
      <DialoguesMenu />
    </div>
  );
};

export default LeftMenu;
