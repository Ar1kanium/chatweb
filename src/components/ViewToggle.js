import React from "react";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useSelector } from "react-redux";

const ViewToggle = ({ status, toggleHandler }) => {
  const view = useSelector((state) => state.menu.view);

  if (view === "default") return <></>;

  return (
    <FontAwesomeIcon
      icon={status ? faChevronLeft : faChevronRight}
      className={status ? "menu__toggle_inactive" : "menu__toggle_active"}
      onClick={toggleHandler}
    />
  );
};

export default ViewToggle;
