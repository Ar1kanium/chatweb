import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { sendImage } from "../services/databaseService";
import { closeModal, updateMessages } from "../actions/actionCreators";

const ImageSender = () => {
  const dispatch = useDispatch();
  const [image, setImage] = React.useState("");
  const localId = useSelector((state) => state.user.localId);
  const { id, activeTab } = useSelector((state) => state.currData);
  const token = useSelector((state) => state.auth.token);

  const handleChange = (e) => {
    if (e.target.value.length > 1) setImage(e.target.value);
  };

  const handleClick = () => {
    sendImage(id, token, localId, image);
    dispatch(closeModal(activeTab));
    setTimeout(() => dispatch(updateMessages(localId, token, id)), 500);
  };

  if (activeTab === "pending") return <div>Начните диалог с приветствия.</div>;

  return (
    <div className="modal__image_wrapper">
      <img
        src={image}
        alt="Загружаемое изображение"
        className="content__image modal__image"
      />
      <input
        className="settings__input"
        placeholder="Скопируйте ссылку на изображение"
        onChange={handleChange}
        value={image}
      />
      {image ? (
        <button className="settings__button" onClick={handleClick}>
          {" "}
          Отправить{" "}
        </button>
      ) : (
        <></>
      )}
    </div>
  );
};

export default ImageSender;
