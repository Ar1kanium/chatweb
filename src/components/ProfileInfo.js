import React from "react";
import { Form, Field } from "react-final-form";
import { useDispatch, useSelector } from "react-redux";
import { closeModal, updateProfileSettings } from "../actions/actionCreators";
import { updateProfile } from "../services/databaseService";
import { toast } from "react-toastify";

const ProfileInfo = () => {
  const avatar = useSelector((state) => state.user.avatar);
  const name = useSelector((state) => state.user.name);
  const dispatch = useDispatch();
  const token = useSelector((state) => state.auth.token);
  const { activeTab } = useSelector((state) => state.currData);
  const { localId } = useSelector((state) => state.user);
  const [currentAvatar, setCurrentAvatar] = React.useState(avatar);
  const [avatarToggleButton, setAvatarToggleButton] = React.useState(true);

  const onSubmit = (values) => {
    if (values.img !== avatar || values.name) {
      dispatch(
        updateProfileSettings(
          currentAvatar,
          values.name ? values.name : name,
          token,
          localId
        )
      );
      updateProfile(
        currentAvatar,
        values.name ? values.name : name,
        token,
        localId
      ).then(() => toast.success("Профиль успешно обновлен"));
      dispatch(closeModal(activeTab));
    }
  };

  const onChange = (e) => {
    if (e.target.value.length > 1) {
      setCurrentAvatar(e.target.value);
      setAvatarToggleButton(true);
    }
  };

  return (
    <Form onSubmit={onSubmit} onChange={onChange}>
      {(props) => (
        <form className="settings__card" onSubmit={props.handleSubmit}>
          <img src={currentAvatar} alt="avatar" className="settings__avatar" />
          <button
            type="button"
            onClick={() => setAvatarToggleButton(false)}
            className={avatarToggleButton ? "settings__button" : "hide"}
          >
            {" "}
            Загрузить{" "}
          </button>
          <Field
            name="img"
            component="input"
            type="text"
            placeholder="Скопируйте ссылку на изображение"
            className={avatarToggleButton ? "hide" : "settings__input"}
            onChange={props.onChange}
          />
          <label htmlFor="name" className="settings__label">
            {" "}
            Ваше имя{" "}
          </label>
          <Field
            name="name"
            component="input"
            type="text"
            autoComplete="username"
            placeholder={name}
            className="settings__input"
          />
          <button className="settings__button" type="submit">
            Обновить профиль
          </button>
        </form>
      )}
    </Form>
  );
};

export default ProfileInfo;
