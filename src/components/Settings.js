import React from "react";
import ProfileInfo from "./ProfileInfo";
import DialoguesSettings from "./DialoguesSettings";

const Settings = () => {
  const [tab, setTab] = React.useState("profile");

  const tabList = {
    profile: <ProfileInfo />,
    dialogues: <DialoguesSettings />,
  };

  return (
    <>
      <div className="settings__header">
        <div
          className={
            tab === "profile"
              ? "settings__tab settings__tab_active"
              : "settings__tab"
          }
          onClick={() => setTab("profile")}
        >
          Профиль
        </div>
        <div
          className={
            tab === "dialogues"
              ? "settings__tab settings__tab_active"
              : "settings__tab"
          }
          onClick={() => setTab("dialogues")}
        >
          Диалоги
        </div>
      </div>
      <div className="settings__card_wrapper">{tabList[tab]}</div>
    </>
  );
};

export default Settings;
