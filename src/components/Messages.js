import React from "react";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import "moment/locale/ru";
import TypingStatus from "./TypingStatus";
import {
  fetchMoreDialogues,
  finishFocusedDialogue,
  setActiveTab,
  updateMessages,
} from "../actions/actionCreators";

const Messages = () => {
  const dialogueId = useSelector((state) => state.currData.id);
  const data = useSelector((state) => state.currData.data[dialogueId]);
  const messageList = data?.messages ? data.messages : {};
  const lastChange = data.timestamp;
  const activeTab = useSelector((state) => state.currData.activeTab);
  const localId = useSelector((state) => state.user.localId);
  const token = useSelector((state) => state.auth.token);
  const dispatch = useDispatch();
  const messagesEndRef = React.useRef(null);
  const containerRef = React.useRef(null);
  React.useEffect(() => {
    const timer = setTimeout(() => {
      if (localId && token && activeTab === "active" && dialogueId) {
        dispatch(updateMessages(localId, token, dialogueId));
      }
    }, 30000);
    return () => clearTimeout(timer);
  });
  React.useEffect(() => {
    containerRef.current.scrollTop = containerRef.current.scrollHeight
  });
  React.useEffect(() => {
    const handleFinishingDialogue = () => {
      if (data.hasOwnProperty("rating") && activeTab === "active") {
        dispatch(finishFocusedDialogue());
        dispatch(setActiveTab("active"));
        dispatch(fetchMoreDialogues("active", token, localId, 1));
      }
    };
    handleFinishingDialogue();
  }, [data, dispatch, activeTab, localId, token]);


  const timeDisplay = (unitTime) => {
    const now = moment();
    const messageTime = moment(unitTime);
    const timediff = now.diff(messageTime);
    return timediff > 21600000
      ? messageTime.locale("ru").calendar({ sameElse: "LLL" })
      : moment(unitTime).locale("ru").from(now) === "Invalid date"
      ? "несколько секунд назад"
      : moment(unitTime).locale("ru").from(now);
  };
  const finishTime =
    activeTab === "finished" || activeTab === "saved"
      ? `Диалог завершен ${timeDisplay(lastChange).toLowerCase()}`
      : "";

  return (
    <div className="content__messages_container" ref={containerRef}>
      {Object.keys(messageList)
        .sort(
          (a, b) => messageList[a]["timestamp"] - messageList[b]["timestamp"]
        )
        .map((el, ind) => {
          return (
            <div key={ind} className="content__message_container">
              <div
                className={
                  messageList[el].author === "client"
                    ? "content__client_message_wrapper"
                    : "content__operator_message_wrapper"
                }
              >
                {messageList[el].image ? (
                  <img
                    className="content__image"
                    src={messageList[el].message}
                    alt="Изображение"
                  />
                ) : (
                  messageList[el].message
                )}
              </div>
              <div
                className={
                  messageList[el].author === "client"
                    ? "content__client_time"
                    : "content__operator_time"
                }
              >
                {timeDisplay(+messageList[el].timestamp)}
              </div>
            </div>
          );
        })}
      <TypingStatus />
      <div ref={messagesEndRef} className="content__finishMessage">
        {finishTime ? finishTime : ""}
      </div>
    </div>
  );
};

export default Messages;
