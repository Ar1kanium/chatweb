import React from "react";
import { useFormik } from "formik";
import { connect, useDispatch } from "react-redux";
import { loginUserAction, signInViaGoogle } from "../actions/actionCreators";
import { getFormError } from "redux-form";
import { Link, Redirect } from "react-router-dom";
import { signInWithGoogle } from "../services/authenticationService";
import ProjectInfo from "./ProjectInfo";

const validate = (values) => {
  const errors = {};
  if (values.email === "")
    errors.email = "Необходимо ввести адрес электронной почты";
  if (values.password === "") errors.password = "Необходимо ввести пароль";
  return errors;
};

const LoginForm = (props) => {
  const dispatch = useDispatch();
  const googleLogin = async () => {
    try {
      const data = await signInWithGoogle();
      dispatch(signInViaGoogle(data));
    } catch (e) {
      console.log(e);
    }
  };

  const formik = useFormik({
    initialValues: {
      email: props.mail ? props.mail : "",
      password: "",
    },
    validate,
    onSubmit: (values) => {
      dispatch(loginUserAction(values));
    },
  });

  if (props.token && !props.error) return <Redirect to="/chat" />;

  return (
    <form className="auth-form" onSubmit={formik.handleSubmit}>
      <ProjectInfo/>
      <span className="auth-form__title">Авторизация</span>
      <label className="auth-form__label" htmlFor="email">
        Электронная почта
      </label>
      <input
        className="auth-form__input"
        id="email"
        name="email"
        type="email"
        autoComplete='username'
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values.email}
      />
      <label className="auth-form__label" htmlFor="password">
        Пароль
      </label>
      <input
        className="auth-form__input"
        id="password"
        name="password"
        type="password"
        autoComplete='current-password'
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values.password}
      />
      {props.errors && (
        <strong className="auth-form__error">{props.errors}</strong>
      )}
      <button className="auth-form__button" type="submit">
        Войти
      </button>
      <button className="auth-form__button" type="button" onClick={googleLogin}>
        {" "}
        Войти, используя Google аккаунт
      </button>
      <div className="auth-form__link_container">
        <Link className="auth-form__link" to="/registration">
          Регистрация
        </Link>
        <Link className="auth-form__link" to="/forgot_pass">
          Забыли пароль?
        </Link>
      </div>
    </form>
  );
};

export default connect((state) => ({
  errors: getFormError("login")(state),
  token: state.auth.token,
  error: state.user.error,
  mail: state.auth.typedEmail,
}))(LoginForm);
